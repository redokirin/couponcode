<?php

class CouponTicket
{

    const COUPON_CODE = 'RUTETEST';

    const COUPON_ID ='0123456789';

    const TICKET_SEQ = COUPON_ID.'_TICKET_SEQ';

    const SEQ_START = 1;

    private $ticketSeq ;

    private $couponCode = self::COUPON_CODE;

    private $couponId = self::COUPON_ID;

    private $redis;

    private $debug = false;

public function __construct()
{
    $this->connectRedis();
}

public function setTicketSeq()
{
  $this->ticketSeq = $this->couponId.'_TICKET_SEQ';
}

public function setCouponCode($code)
{
  $this->couponCode = $code;
}

public function setCouponId($id)
{
  $this->couponId = $id;
}

public function getTicketSeq()
{
  $seq = $this->redis->exists($this->ticketSeq)
    ? $this->redis->incr($this->ticketSeq)
    : $this->setTicketQueue();

  return $seq;
}

public function setTicketQueue(){
  echo 'setTicketQueue';
  $this->redis->set($this->ticketSeq,self::SEQ_START);
  return $this->redis->get($this->ticketSeq);
}

// 設定佇列內容
public function doSetTicketQueue($ct)
{
  for($i=0;$i<=$ct;$i++){
    $this->redis->rpush('TICKET_QUEUE',$this->getCouponTicket());
  }
}

//取得佇列內容
function doGetTicketQueue()
{
  return $this->redis->lpop('TICKET_QUEUE');
}

// 取得折價券票券號碼
public function getCouponTicket()
{
  $coupon_ticket_format = '%s%08d';

  $seq = $this->getTicketSeq();
  return sprintf($coupon_ticket_format,$this->couponId,$seq);
}


// 連接Redis
private function connectRedis()
{
  try{
    //Connecting to Redis server on localhost
    $this->redis = new Redis();
    $this->redis->connect('redis', 6379,60);    

  }catch(RedisException $e){
    var_dump($e);
  }
}

// 顯示 Redis 資訊
public function showRedisInfo()
{
  echo "<pre><p> ======== connection ======== </p>";
  echo "Connection to server sucessfully";
  echo "<br>";
  echo "Server is running: ";
  echo "ping:" .$this->redis->ping();
  echo "<p> ======== connection ======== </p>";  
  print_r($this->redis->info());
}

// 顯示佇列長度
public function showQueueLength()
{
  echo '<br>';
  echo 'QUEUE LENGTH:'.$this->redis->llen('TICKET_QUEUE');
  echo '<br>';
}

// 遞增測試
// public function testRedisIncr($seq=1)
// {
//   $output_string = '%s:%s%010d<br>';
//   // echo '<pre>';
  
//   if(!$this->redis->exists(self::TICKET_SEQ)){
//     $this->redis->set(self::TICKET_SEQ,$seq);
//   }
//   else{
//     $seq = $this->redis->get(self::TICKET_SEQ);
//   }

//   echo sprintf($output_string,self::TICKET_SEQ,$this->couponCode,$seq);
  
//   // echo $this->redis->get(TICKET_SEQ);
  
//   for($i=0;$i<100;$i++){
//     $seq = $this->getTicketSeq();

//     echo sprintf($output_string,self::TICKET_SEQ,$this->couponCode,$seq);
//   }
// }

// public function testRedisHash()
// {
//   $obj = [
//     'coupon_id'   => '12345',
//     'ctrl_rowid'  =>  '23456789',
//     'transaction_no'  => '9876543',
//     'use_status'  =>  '0'
//   ];

//   $this->redis->hmset('TICKET_DETAIL',$obj);

//   print_r ($this->redis->hmget(
//     'TICKET_DETAIL',
//     ['coupon_id','ctrl_rowid'])
//   );

//   print_r($this->redis->hgetall('TICKET_DETAIL'));
// }
}