<?php

class CouponCode{

    //
    const CHAR_POOL = 'ABCDEFGHGKLMNPQRSTUVWXYZ23456789';

    // 是否檢查排除詞
    private $isCheckExcludeWord = true;

    // 是否檢查重複
    private $isCheckDuplicate = true;

    // 是否檢查連續三個同字元
    private $isCheckTripleChar = true;
    
    // 折價券長度 預設10字元
    private $couponCodeLength = 10;

    // 折價券產出筆數 預設1筆
    private $couponRows = 1;

    public function setCouponRows($rows){
        $this->couponRows = $rows;
    }

    public function setCouponCodeLength($length){
        $this->couponCodeLength = $length;
    }

    public function doGenerateCouponCode()
    {
        $rows = $this->couponRows;
        $length = $this->couponCodeLength;

        $result = [];
        if($rows < 1){
            return $result;
        }
        
        do{
            $doSet = true;
            $temp_str = self::doGetRandomString($length);

            // 已產生的紀錄是否有重複
            if($this->isCheckDuplicate == true && isset($result[$temp_str])){
                $doSet == false;
            }

            // 是否包含排除詞
            if($this->isCheckExcludeWord == true && self::doCheckExcludeWord($temp_str)){
                $doSet == false;
            }

            if($doSet == true){
                $result[$temp_str] = true;
                $rows--;
            }

        }while($rows > 0);
        
        return array_keys($result);
    }

    // 
    public function doGetRandomString($length)
    {
        $source_string = self::CHAR_POOL;
        $strleng = strlen($source_string);
        $result_string = '';
    
        while(strlen($result_string) < $length){
    
            $pos = rand(0,$strleng);
            $pos_hit = substr(self::CHAR_POOL,$pos,1);
            $doFit = true;
            
            // 檢查是否產出連三同樣字元
            if($this->isCheckTripleChar == true){
                $preStr = substr($result_string, -1);
                $mark = ($pos_hit == $preStr)?1:0;
            
                $doFit = (($mark == 0) && ($pos_hit != $preStr))
                    ?true:false;
            }

            if($doFit == true){
                $result_string .= $pos_hit;
            }
        }
        
        return $result_string;   
    }
    
    // 
    static public function doCheckExcludeWord($string)
    {
        $exclude = self::getExcludeList();
    
        foreach($exclude as $excludeWord){
            if(strpos($string,$excludeWord)){
                return $excludeWord;
                break;
            }
        }
        return false;
    }
    
    // 
    static public function getExcludeList(){
    
        $exclude = [
            'KMT',
            'DPP',
            'SHOPEE',
            'TAOBAO',
            'MOMO',
            'YAHOO',
            'TMAIL',
            'A55',
            'ALAN',
            'ARSE',
            'ASS',
            'BAD',
            'BENT',
            'BITCH',
            'BOOB',
            'BUM',
            'CLIT',
            'CRAP',
            'CUNT',
            'DAMM',
            'DEAD',
            'DEEN',
            'DICK',
            'DORK',
            'FAG',
            'FART',
            'FUCK',
            'GAY',
            'GOD',
            'HATE',
            'HATK',
            'HOBB',
            'JERK',
            'JOCK',
            'KNOB',
            'LICK',
            'MAD',
            'MUFF',
            'NEAK',
            'NOB',
            'PANT',
            'PENIS',
            'RALPH',
            'RANK',
            'RAPE',
            'SEX',
            'SHAG',
            'SHIT',
            'SUCK',
            'TIT',
            'WANK',
            'WORD'
        ];
    
        return $exclude;
    }
}